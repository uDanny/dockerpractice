package com.example.welcomeNode.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class RestService {
    private static final String URL = "http://springboot-docker-compose-second-app-container:8081/get";
    private final RestTemplate restTemplate;

    @Autowired
    public RestService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public String getApi() {
        return restTemplate.getForObject(URL, String.class);
    }
}
