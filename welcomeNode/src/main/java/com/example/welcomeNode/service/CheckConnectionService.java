package com.example.welcomeNode.service;

import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

@Service
public class CheckConnectionService {

    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost:3306/asd";

    //  Database credentials
    static final String USER = "asd";
    static final String PASS = "asd";

    public void checkConnection() {
        try (Connection con = DriverManager.getConnection(
                "jdbc:mysql://docker-database:3306/asd", "asd", "asd")) {
            Class.forName("com.mysql.jdbc.Driver");

            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select * from table_name");
            while (rs.next())
                System.out.println(rs.getInt(1) + "  " + rs.getString(2) + "  " + rs.getString(3));
        } catch (Exception e) {
            System.out.println(e);
        }


    }
}
