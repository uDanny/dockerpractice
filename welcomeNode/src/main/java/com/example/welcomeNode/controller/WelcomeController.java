package com.example.welcomeNode.controller;

import com.example.welcomeNode.service.CheckConnectionService;
import com.example.welcomeNode.service.RestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WelcomeController {
    @Autowired
    private RestService restService;

    @Autowired
    private CheckConnectionService connectionService;

    @GetMapping("/")
    public String getRoot() {
        System.out.println("Welcome");
        connectionService.checkConnection();
        return "Welcome2";
    }

    @GetMapping("/getApi")
    public String getApi() {
        System.out.println("thorough api");
        connectionService.checkConnection();
        return restService.getApi();
    }
}
