package com.example.secondnode.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ApiController {
    @GetMapping("/")
    public String getRoot() {
        return "Welcome from API";
    }
    @GetMapping("/get")
    public String getApi() {
        return "apiResponse";
    }
}
